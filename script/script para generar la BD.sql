-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 17-06-2020 a las 02:33:22
-- Versión del servidor: 5.5.24-log
-- Versión de PHP: 5.4.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `gendra01`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `asentammiento`
--

CREATE TABLE IF NOT EXISTS `asentammiento` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  `tipo_asentamiento` varchar(255) NOT NULL,
  `tipo_zona` varchar(255) NOT NULL,
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `asentammiento`
--

INSERT INTO `asentammiento` (`id`, `nombre`, `tipo_asentamiento`, `tipo_zona`, `version`) VALUES
(1, 'condesa', 'colonia', 'urbano', 0),
(2, 'Ciudad Cuauhtémoc Sección Quetzalcoatl', 'Colonia', 'Urbano', 0),
(3, 'San Cristobal Centro', 'colonia', 'Urbano', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `codigo_postal`
--

CREATE TABLE IF NOT EXISTS `codigo_postal` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `codigo` varchar(255) NOT NULL,
  `estado` varchar(255) NOT NULL,
  `localidad` varchar(255) NOT NULL,
  `municipio` varchar(255) NOT NULL,
  `version` int(11) DEFAULT NULL,
  `asentamiento_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK1r3jn0jo86f70ic6nqsuc5agq` (`asentamiento_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `codigo_postal`
--

INSERT INTO `codigo_postal` (`id`, `codigo`, `estado`, `localidad`, `municipio`, `version`, `asentamiento_id`) VALUES
(1, '06140', 'ciudad de mexico', 'ciudad de mexico', 'cuauhtemoc', 2, 1),
(2, '55067', 'México', 'Ecatepec de Morelos', 'Ecatepec de Morelos', 0, 2),
(3, '55000', 'México', 'Ecatepec de Morelos', 'Ecatepec de Morelos', 0, 3);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `codigo_postal`
--
ALTER TABLE `codigo_postal`
  ADD CONSTRAINT `FK1r3jn0jo86f70ic6nqsuc5agq` FOREIGN KEY (`asentamiento_id`) REFERENCES `asentammiento` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
