package com.gendra.dto;

public class CodigoPostalDTO {
	
	private String codigo;
	private String localidad;
	private String estado;
	private String nombre;
	private String tipoZona;
	private String tipoAsentamiento;
	private String municipio;
	
	
	public CodigoPostalDTO(String codigo, String localidad, String estado, String nombre, String tipoZona,
			String tipoAsentamiento, String municipio) {
		super();
		this.codigo = codigo;
		this.localidad = localidad;
		this.estado = estado;
		this.nombre = nombre;
		this.tipoZona = tipoZona;
		this.tipoAsentamiento = tipoAsentamiento;
		this.municipio = municipio;
	}


	public String getCodigo() {
		return codigo;
	}


	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}


	public String getLocalidad() {
		return localidad;
	}


	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}


	public String getEstado() {
		return estado;
	}


	public void setEstado(String estado) {
		this.estado = estado;
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public String getTipoZona() {
		return tipoZona;
	}


	public void setTipoZona(String tipoZona) {
		this.tipoZona = tipoZona;
	}


	public String getTipoAsentamiento() {
		return tipoAsentamiento;
	}


	public void setTipoAsentamiento(String tipoAsentamiento) {
		this.tipoAsentamiento = tipoAsentamiento;
	}


	public String getMunicipio() {
		return municipio;
	}


	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}


	@Override
	public String toString() {
		return "codigoPostalDTO [codigo=" + codigo + ", localidad=" + localidad + ", estado=" + estado + ", nombre="
				+ nombre + ", tipoZona=" + tipoZona + ", tipoAsentamiento=" + tipoAsentamiento + ", municipio="
				+ municipio + "]";
	}
	
	
	

}
