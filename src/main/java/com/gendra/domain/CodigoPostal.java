package com.gendra.domain;
import org.springframework.roo.addon.javabean.annotations.RooEquals;
import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooToString;
import org.springframework.roo.addon.jpa.annotations.entity.RooJpaEntity;

import io.springlets.format.EntityFormat;

import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

/**
 * = CodigoPostal
 TODO Auto-generated class documentation
 *
 */
@RooJavaBean
@RooToString
@RooJpaEntity
@RooEquals(isJpaEntity = true)
@Table(name = "codigoPostal")
public class CodigoPostal {

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    @Version
    private Integer version;

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    @NotNull
    private String codigo;

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    @NotNull
    private String localidad;

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    @NotNull
    private String estado;

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    @NotNull
    private String municipio;
    
    /** El asentamiento. */
    @ManyToOne
    private Asentammiento asentamiento;
}
