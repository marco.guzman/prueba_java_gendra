package com.gendra.web;
import com.gendra.domain.CodigoPostal;
import org.springframework.roo.addon.web.mvc.controller.annotations.config.RooJsonMixin;

/**
 * = CodigoPostalJsonMixin
 TODO Auto-generated class documentation
 *
 */
@RooJsonMixin(entity = CodigoPostal.class)
public abstract class CodigoPostalJsonMixin {
}
