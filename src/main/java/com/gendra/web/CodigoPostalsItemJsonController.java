package com.gendra.web;
import com.gendra.domain.CodigoPostal;
import org.springframework.roo.addon.web.mvc.controller.annotations.ControllerType;
import org.springframework.roo.addon.web.mvc.controller.annotations.RooController;
import org.springframework.roo.addon.web.mvc.controller.annotations.responses.json.RooJSON;

/**
 * = CodigoPostalsItemJsonController
 TODO Auto-generated class documentation
 *
 */
@RooController(entity = CodigoPostal.class, pathPrefix = "/api", type = ControllerType.ITEM)
@RooJSON
public class CodigoPostalsItemJsonController {
}
