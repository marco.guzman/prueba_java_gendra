package com.gendra.web;
import com.gendra.domain.CodigoPostal;
import com.gendra.service.api.CodigoPostalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jackson.JsonObjectDeserializer;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.convert.ConversionService;
import org.springframework.roo.addon.web.mvc.controller.annotations.config.RooDeserializer;

/**
 * = CodigoPostalDeserializer
 TODO Auto-generated class documentation
 *
 */
@RooDeserializer(entity = CodigoPostal.class)
public class CodigoPostalDeserializer extends JsonObjectDeserializer<CodigoPostal> {

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    private CodigoPostalService codigoPostalService;

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    private ConversionService conversionService;

    /**
     * TODO Auto-generated constructor documentation
     *
     * @param codigoPostalService
     * @param conversionService
     */
    @Autowired
    public CodigoPostalDeserializer(@Lazy CodigoPostalService codigoPostalService, ConversionService conversionService) {
        this.codigoPostalService = codigoPostalService;
        this.conversionService = conversionService;
    }
}
