package com.gendra.web;
import com.gendra.domain.CodigoPostal;
import org.springframework.roo.addon.web.mvc.controller.annotations.ControllerType;
import org.springframework.roo.addon.web.mvc.controller.annotations.RooController;
import org.springframework.roo.addon.web.mvc.thymeleaf.annotations.RooThymeleaf;

/**
 * = CodigoPostalsCollectionThymeleafController
 TODO Auto-generated class documentation
 *
 */
@RooController(entity = CodigoPostal.class, type = ControllerType.COLLECTION)
@RooThymeleaf
public class CodigoPostalsCollectionThymeleafController {
}
