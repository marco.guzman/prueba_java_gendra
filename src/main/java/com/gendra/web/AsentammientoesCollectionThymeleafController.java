package com.gendra.web;
import com.gendra.domain.Asentammiento;
import org.springframework.roo.addon.web.mvc.controller.annotations.ControllerType;
import org.springframework.roo.addon.web.mvc.controller.annotations.RooController;
import org.springframework.roo.addon.web.mvc.thymeleaf.annotations.RooThymeleaf;

/**
 * = AsentammientoesCollectionThymeleafController
 TODO Auto-generated class documentation
 *
 */
@RooController(entity = Asentammiento.class, type = ControllerType.COLLECTION)
@RooThymeleaf
public class AsentammientoesCollectionThymeleafController {
}
