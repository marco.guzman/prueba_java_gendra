package com.gendra.web;
import com.gendra.domain.CodigoPostal;
import com.gendra.dto.CodigoPostalDTO;
import com.gendra.service.api.CodigoPostalService;

import io.springlets.data.domain.GlobalSearch;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.roo.addon.web.mvc.controller.annotations.ControllerType;
import org.springframework.roo.addon.web.mvc.controller.annotations.RooController;
import org.springframework.roo.addon.web.mvc.controller.annotations.responses.json.RooJSON;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * = CodigoPostalsCollectionJsonController
 TODO Auto-generated class documentation
 *
 */
@RooController(entity = CodigoPostal.class, pathPrefix = "/api", type = ControllerType.COLLECTION)
@RooJSON
public class CodigoPostalsCollectionJsonController {
	
	private CodigoPostalService codigoPostalService;
	
	@GetMapping(value = "/code/{codigo}")
	public ResponseEntity<List<CodigoPostalDTO>> buscarCodigoPostal(@PathVariable String codigo) {
		
		 List<CodigoPostalDTO> codigoPostals = getCodigoPostalService().findByIdCodigo(codigo);
	        return ResponseEntity.ok(codigoPostals);
		//return new ResponseEntity<List<CodigoPostalDTO>>(codigoPostalService.findByIdCodigo(codigo), HttpStatus.OK);
	}
}
