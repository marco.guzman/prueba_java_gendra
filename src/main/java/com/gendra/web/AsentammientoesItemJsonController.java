package com.gendra.web;
import com.gendra.domain.Asentammiento;
import org.springframework.roo.addon.web.mvc.controller.annotations.ControllerType;
import org.springframework.roo.addon.web.mvc.controller.annotations.RooController;
import org.springframework.roo.addon.web.mvc.controller.annotations.responses.json.RooJSON;

/**
 * = AsentammientoesItemJsonController
 TODO Auto-generated class documentation
 *
 */
@RooController(entity = Asentammiento.class, pathPrefix = "/api", type = ControllerType.ITEM)
@RooJSON
public class AsentammientoesItemJsonController {
}
