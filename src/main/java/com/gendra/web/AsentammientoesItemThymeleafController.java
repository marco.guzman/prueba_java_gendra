package com.gendra.web;
import com.gendra.domain.Asentammiento;
import io.springlets.web.mvc.util.concurrency.ConcurrencyManager;
import org.springframework.roo.addon.web.mvc.controller.annotations.ControllerType;
import org.springframework.roo.addon.web.mvc.controller.annotations.RooController;
import org.springframework.roo.addon.web.mvc.thymeleaf.annotations.RooThymeleaf;

/**
 * = AsentammientoesItemThymeleafController
 TODO Auto-generated class documentation
 *
 */
@RooController(entity = Asentammiento.class, type = ControllerType.ITEM)
@RooThymeleaf
public class AsentammientoesItemThymeleafController implements ConcurrencyManager<Asentammiento> {
}
