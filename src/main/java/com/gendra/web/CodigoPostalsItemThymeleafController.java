package com.gendra.web;
import com.gendra.domain.CodigoPostal;
import io.springlets.web.mvc.util.concurrency.ConcurrencyManager;
import org.springframework.roo.addon.web.mvc.controller.annotations.ControllerType;
import org.springframework.roo.addon.web.mvc.controller.annotations.RooController;
import org.springframework.roo.addon.web.mvc.thymeleaf.annotations.RooThymeleaf;

/**
 * = CodigoPostalsItemThymeleafController
 TODO Auto-generated class documentation
 *
 */
@RooController(entity = CodigoPostal.class, type = ControllerType.ITEM)
@RooThymeleaf
public class CodigoPostalsItemThymeleafController implements ConcurrencyManager<CodigoPostal> {
}
