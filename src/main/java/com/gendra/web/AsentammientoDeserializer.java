package com.gendra.web;
import com.gendra.domain.Asentammiento;
import com.gendra.service.api.AsentammientoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jackson.JsonObjectDeserializer;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.convert.ConversionService;
import org.springframework.roo.addon.web.mvc.controller.annotations.config.RooDeserializer;

/**
 * = AsentammientoDeserializer
 TODO Auto-generated class documentation
 *
 */
@RooDeserializer(entity = Asentammiento.class)
public class AsentammientoDeserializer extends JsonObjectDeserializer<Asentammiento> {

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    private AsentammientoService asentammientoService;

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    private ConversionService conversionService;

    /**
     * TODO Auto-generated constructor documentation
     *
     * @param asentammientoService
     * @param conversionService
     */
    @Autowired
    public AsentammientoDeserializer(@Lazy AsentammientoService asentammientoService, ConversionService conversionService) {
        this.asentammientoService = asentammientoService;
        this.conversionService = conversionService;
    }
}
