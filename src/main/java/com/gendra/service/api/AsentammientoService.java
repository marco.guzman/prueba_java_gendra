package com.gendra.service.api;
import com.gendra.domain.Asentammiento;
import io.springlets.data.web.validation.ValidatorService;
import io.springlets.format.EntityResolver;
import org.springframework.roo.addon.layers.service.annotations.RooService;

/**
 * = AsentammientoService
 TODO Auto-generated class documentation
 *
 */
@RooService(entity = Asentammiento.class)
public interface AsentammientoService extends EntityResolver<Asentammiento, Long>, ValidatorService<Asentammiento> {
}
