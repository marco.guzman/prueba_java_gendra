package com.gendra.service.api;
import com.gendra.domain.CodigoPostal;
import com.gendra.dto.CodigoPostalDTO;

import io.springlets.data.web.validation.ValidatorService;
import io.springlets.format.EntityResolver;

import java.util.List;

import org.springframework.roo.addon.layers.service.annotations.RooService;

/**
 * = CodigoPostalService
 TODO Auto-generated class documentation
 *
 */
@RooService(entity = CodigoPostal.class)
public interface CodigoPostalService extends EntityResolver<CodigoPostal, Long>, ValidatorService<CodigoPostal> {
	
	public abstract List<CodigoPostalDTO> findByIdCodigo(String codigo);
}
