package com.gendra.service.impl;
import com.gendra.domain.CodigoPostal;
import com.gendra.dto.CodigoPostalDTO;
import com.gendra.repository.CodigoPostalRepository;
import com.gendra.service.api.CodigoPostalService;

import io.springlets.data.domain.GlobalSearch;
import io.springlets.data.web.validation.MessageI18n;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.roo.addon.layers.service.annotations.RooServiceImpl;

/**
 * = CodigoPostalServiceImpl
 TODO Auto-generated class documentation
 *
 */
@RooServiceImpl(service = CodigoPostalService.class)
public class CodigoPostalServiceImpl implements CodigoPostalService {
	

	private CodigoPostalRepository codigoPostalRepository;
	
	@Transactional
	@Override
	public List<CodigoPostalDTO> findByIdCodigo(String codigo) {
		// TODO Auto-generated method stub
		System.out.println("###### EJECUTANDO EL METODO SERVICE IMPL#####");
   		System.out.println("JSON SERVICE IMPL: " + codigo);
		List<Object[]> codipostal = getCodigoPostalRepository().busquedaCodigoPostal(codigo);
		ArrayList<CodigoPostalDTO>  codigoPostalDTO = new ArrayList<CodigoPostalDTO>();
		for(Object[]c : codipostal){
			codigoPostalDTO.add(new CodigoPostalDTO(c[0].toString(),c[1].toString(),c[2].toString(),
					c[3].toString(),c[4].toString(),c[5].toString(),c[6].toString()));
					}
		return codigoPostalDTO;
	}

	
}
