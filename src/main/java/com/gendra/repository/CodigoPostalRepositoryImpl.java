package com.gendra.repository;

import io.springlets.data.jpa.repository.support.QueryDslRepositorySupportExt;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.roo.addon.layers.repository.jpa.annotations.RooJpaRepositoryCustomImpl;
import com.gendra.domain.CodigoPostal;

/**
 * = CodigoPostalRepositoryImpl
 *
 * Implementation of CodigoPostalRepositoryCustom
 *
 */
@RooJpaRepositoryCustomImpl(repository = CodigoPostalRepositoryCustom.class)
public class CodigoPostalRepositoryImpl extends QueryDslRepositorySupportExt<CodigoPostal> implements CodigoPostalRepositoryCustom{

    /**
     * Default constructor
     */
    CodigoPostalRepositoryImpl() {
        super(CodigoPostal.class);
    }

	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> busquedaCodigoPostal(String codigo) {
		// TODO Auto-generated method stub
		List<Object[]> cpostal = new ArrayList<Object[]>();
		System.out.println("-- executing query --");
		try {
   	      EntityManager em = getEntityManager();
   	      String jpql = " SELECT co.codigo, co.localidad, co.estado, co.municipio, ase.nombre, ase.tipoZona, ase.tipoAsentamiento "
   	    		  + " FROM CodigoPostal co JOIN co.asentamiento ase "   	      		
   	    		  + " WHERE co.codigo = "+codigo+""; 
   	      Query query  = em.createQuery(jpql);    	    
	      System.out.println("-- EJECUTANDO QUERY --"+jpql);
	      System.out.println("-- RESULTADO --"+ query.getResultList());
	      cpostal = query.getResultList();
		}catch(Exception e){
			System.out.printf("busquedaCodigoPostal(String)", e); //$NON-NLS-1$
		}
		return cpostal;
	}

}

