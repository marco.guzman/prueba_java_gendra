package com.gendra.repository;
import com.gendra.domain.CodigoPostal;

import java.util.List;
import org.springframework.roo.addon.layers.repository.jpa.annotations.RooJpaRepositoryCustom;

/**
 * = CodigoPostalRepositoryCustom
 TODO Auto-generated class documentation
 *
 */
@RooJpaRepositoryCustom(entity = CodigoPostal.class)
public interface CodigoPostalRepositoryCustom {
	
	//gucm
	public abstract List<Object[]> busquedaCodigoPostal(String codigo);
}
