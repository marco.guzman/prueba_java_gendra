package com.gendra.repository;

import io.springlets.data.jpa.repository.support.QueryDslRepositorySupportExt;
import org.springframework.roo.addon.layers.repository.jpa.annotations.RooJpaRepositoryCustomImpl;
import com.gendra.domain.Asentammiento;

/**
 * = AsentammientoRepositoryImpl
 *
 * Implementation of AsentammientoRepositoryCustom
 *
 */
@RooJpaRepositoryCustomImpl(repository = AsentammientoRepositoryCustom.class)
public class AsentammientoRepositoryImpl extends QueryDslRepositorySupportExt<Asentammiento> implements AsentammientoRepositoryCustom{

    /**
     * Default constructor
     */
    AsentammientoRepositoryImpl() {
        super(Asentammiento.class);
    }
}