package com.gendra.repository;
import com.gendra.domain.Asentammiento;
import org.springframework.roo.addon.layers.repository.jpa.annotations.RooJpaRepositoryCustom;

/**
 * = AsentammientoRepositoryCustom
 TODO Auto-generated class documentation
 *
 */
@RooJpaRepositoryCustom(entity = Asentammiento.class)
public interface AsentammientoRepositoryCustom {
}
