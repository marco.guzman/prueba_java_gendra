package com.gendra.repository;
import com.gendra.domain.CodigoPostal;
import io.springlets.data.jpa.repository.DetachableJpaRepository;
import org.springframework.roo.addon.layers.repository.jpa.annotations.RooJpaRepository;

/**
 * = CodigoPostalRepository
 TODO Auto-generated class documentation
 *
 */
@RooJpaRepository(entity = CodigoPostal.class)
public interface CodigoPostalRepository extends DetachableJpaRepository<CodigoPostal, Long>, CodigoPostalRepositoryCustom {
	
	
}
