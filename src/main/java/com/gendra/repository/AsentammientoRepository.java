package com.gendra.repository;
import com.gendra.domain.Asentammiento;
import io.springlets.data.jpa.repository.DetachableJpaRepository;
import org.springframework.roo.addon.layers.repository.jpa.annotations.RooJpaRepository;

/**
 * = AsentammientoRepository
 TODO Auto-generated class documentation
 *
 */
@RooJpaRepository(entity = Asentammiento.class)
public interface AsentammientoRepository extends DetachableJpaRepository<Asentammiento, Long>, AsentammientoRepositoryCustom {
}
