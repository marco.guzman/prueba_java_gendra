// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.gendra.repository;

import com.gendra.domain.Asentammiento;
import com.gendra.qdsl.QAsentamiento;
import com.gendra.repository.AsentammientoRepositoryImpl;
import com.querydsl.core.types.Path;
import com.querydsl.jpa.JPQLQuery;
import io.springlets.data.domain.GlobalSearch;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;

privileged aspect AsentammientoRepositoryImpl_Roo_Jpa_Repository_Impl {
    
    declare @type: AsentammientoRepositoryImpl: @Transactional(readOnly = true);
    
    /**
     * TODO Auto-generated attribute documentation
     * 
     */
    public static final String AsentammientoRepositoryImpl.NOMBRE = "nombre";
    
    /**
     * TODO Auto-generated attribute documentation
     * 
     */
    public static final String AsentammientoRepositoryImpl.TIPO_ZONA = "tipoZona";
    
    /**
     * TODO Auto-generated attribute documentation
     * 
     */
    public static final String AsentammientoRepositoryImpl.TIPO_ASENTAMIENTO = "tipoAsentamiento";
    
    /**
     * TODO Auto-generated method documentation
     * 
     * @param globalSearch
     * @param pageable
     * @return Page
     */
    public Page<Asentammiento> AsentammientoRepositoryImpl.findAll(GlobalSearch globalSearch, Pageable pageable) {
        
        QAsentamiento asentammiento = QAsentamiento.asentammiento;
        
        JPQLQuery<Asentammiento> query = from(asentammiento);
        
        Path<?>[] paths = new Path<?>[] {asentammiento.nombre,asentammiento.tipoZona,asentammiento.tipoAsentamiento};        
        applyGlobalSearch(globalSearch, query, paths);
        
        AttributeMappingBuilder mapping = buildMapper()
			.map(NOMBRE, asentammiento.nombre)
			.map(TIPO_ZONA, asentammiento.tipoZona)
			.map(TIPO_ASENTAMIENTO, asentammiento.tipoAsentamiento);
        
        applyPagination(pageable, query, mapping);
        applyOrderById(query);
        
        return loadPage(query, pageable, asentammiento);
    }
    
    /**
     * TODO Auto-generated method documentation
     * 
     * @param ids
     * @param globalSearch
     * @param pageable
     * @return Page
     */
    public Page<Asentammiento> AsentammientoRepositoryImpl.findAllByIdsIn(List<Long> ids, GlobalSearch globalSearch, Pageable pageable) {
        
        QAsentamiento asentammiento = QAsentamiento.asentammiento;
        
        JPQLQuery<Asentammiento> query = from(asentammiento);
        
        Path<?>[] paths = new Path<?>[] {asentammiento.nombre,asentammiento.tipoZona,asentammiento.tipoAsentamiento};        
        applyGlobalSearch(globalSearch, query, paths);
        
        // Also, filter by the provided ids
        query.where(asentammiento.id.in(ids));
        
        AttributeMappingBuilder mapping = buildMapper()
			.map(NOMBRE, asentammiento.nombre)
			.map(TIPO_ZONA, asentammiento.tipoZona)
			.map(TIPO_ASENTAMIENTO, asentammiento.tipoAsentamiento);
        
        applyPagination(pageable, query, mapping);
        applyOrderById(query);
        
        return loadPage(query, pageable, asentammiento);
    }
    
}
