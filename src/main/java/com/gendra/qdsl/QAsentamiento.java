package com.gendra.qdsl;

import static com.querydsl.core.types.PathMetadataFactory.*;
import com.querydsl.core.types.dsl.*;
import javax.annotation.Generated;
import com.gendra.domain.Asentammiento;

@Generated("com.querydsl.codegen.EntitySerializer")
public class QAsentamiento extends EntityPathBase<Asentammiento>  {

	private static final long serialVersionUID = -214209042748055014L;

	public QAsentamiento(Class<? extends Asentammiento> type, String variable) {
		super(type, variable);
		// TODO Auto-generated constructor stub
	}
	
	   public QAsentamiento(String variable) {
	        super(Asentammiento.class, forVariable(variable));
	    }

	
	 /** La constante esquema. */
    public static final QAsentamiento asentammiento = new QAsentamiento("asentammiento");
    
    
    /** El id. */
    public final NumberPath<Long> id = createNumber("id", Long.class);


    /** El version. */
    public final NumberPath<Integer> version = createNumber("version", Integer.class);
	
    /** El nombre. */
    public final StringPath nombre = createString("nombre");
    
    /** El tipoZona. */
    public final StringPath tipoZona = createString("tipoZona");
    
    /** El tipoZona. */
    public final StringPath tipoAsentamiento = createString("tipoAsentamiento");
	
	

}
