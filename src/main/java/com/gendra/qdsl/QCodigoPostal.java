package com.gendra.qdsl;

import static com.querydsl.core.types.PathMetadataFactory.*;
import com.querydsl.core.types.dsl.*;
import javax.annotation.Generated;
import com.gendra.domain.CodigoPostal;

@Generated("com.querydsl.codegen.EntitySerializer")
public class QCodigoPostal extends EntityPathBase<CodigoPostal> {
	
	
	private static final long serialVersionUID = 6587560277585117369L;

	public QCodigoPostal(Class<? extends CodigoPostal> type, String variable) {
		super(type, variable);
		// TODO Auto-generated constructor stub
	}
	
	   public QCodigoPostal(String variable) {
	        super(CodigoPostal.class, forVariable(variable));
	    }
	   
	   /** La constante asentammiento. */
	    public static final QCodigoPostal codigoPostal = new QCodigoPostal("codigoPostal");
	   
	    /** El id. */
	    public final NumberPath<Long> id = createNumber("id", Long.class);


	    /** El version. */
	    public final NumberPath<Integer> version = createNumber("version", Integer.class);
		
	    /** El codigo. */
	    public final StringPath codigo = createString("codigo");
	    
	    /** El localidad. */
	    public final StringPath localidad = createString("localidad");
	    
	    /** El estado. */
	    public final StringPath estado = createString("estado");
	    
	    /** El municipio. */
	    public final StringPath municipio = createString("municipio");

}
